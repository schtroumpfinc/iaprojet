package tests;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class BidonTest {
  @Test
  public void evaluatesExpression() {
    Bidon calculator = new Bidon();
    int sum = calculator.evaluate("1+2+3");
    assertEquals(6, sum);
  }
}
