package Games.Kalaha.Players;
import Games.Kalaha.Boards.Board;

/**
 * An evaluation function for the minimax algorithm
 * @author Bury Jason and Barat Kevin
 */
public abstract class BaratBuryHeuristic{
	/** Compute the evaluation value */
	public abstract int compute(Board board, String avatar);

	/** Return the advisable depth for minimax, version for >= 2 players */
	public int getAdvisableDepth(){
		return 8;
	}

	/** Return the advisable depth for an alpha-beta search */
	public int getAdvisableabDepth(){
		return 9;
	}
}
