package Games.Kalaha.Players;
import Games.Kalaha.Boards.Board;
import Games.Kalaha.Move;
import Games.Kalaha.Game;
import Games.Kalaha.Players.Player;
import java.lang.Math;
import java.lang.Math;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import FX.PlayerMaker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.Node;
import javafx.scene.layout.VBox;

/**
 * AI using Minimax algorithm
 * @author Bury Jason and Barat Kevin
 */
public class BaratBuryMiniMaxAI extends Player{
	private static final int PINF = Integer.MAX_VALUE;//+infinite
	private static final int MINF = Integer.MIN_VALUE;//-infinite
	private BaratBuryHeuristic heuristic;
	private int depth;
	private boolean alphabetaMode;

	/**
	 * The maker to choose the heuristic for a Minimax AI in the GUI
	 */
	public static class Maker implements PlayerMaker<Integer, Integer, Board, String, Game, Move, Player>{
		private final RadioButton rbMaxStocks, rbMaxCases, rbMinAdverse, rbMinGap;

		public Maker(){
			ToggleGroup ltg = new ToggleGroup();
			rbMaxStocks = new RadioButton("Maximise ses reserves");
			rbMaxStocks.setToggleGroup(ltg);
			rbMaxCases = new RadioButton("Maximise ses cases");
			rbMaxCases.setToggleGroup(ltg);
			rbMinAdverse = new RadioButton("Minimise les reserves ennemies");
			rbMinAdverse.setToggleGroup(ltg);
			rbMinGap = new RadioButton("Minimise somme carres differences entre ses creux");
			rbMinGap.setToggleGroup(ltg);
			rbMaxCases.setSelected(true);
		}

		@Override
		public Node getConfigPane(){
			VBox ltg = new VBox();
			ltg.getChildren().add(rbMaxStocks);
			ltg.getChildren().add(rbMaxCases);
			ltg.getChildren().add(rbMinAdverse);
			ltg.getChildren().add(rbMinGap);
			return ltg;
		}

		@Override
		public Player getPlayer(){
			BaratBuryHeuristic h;
			if(rbMaxStocks.isSelected()) h = new BaratBuryMaxStocks();
			else if(rbMinAdverse.isSelected()) h = new BaratBuryMinAdverseStocks();
			else if(rbMinGap.isSelected()) h = new BaratBuryMinGap();
			else h = new BaratBuryMaxCases();
			return new BaratBuryMiniMaxAI(h);
		}

		@Override
		public String toString() {
			return "Minimax AI by Barat-Bury";
		}
	}

	private void updateDepth(){
		int advised;
		if(alphabetaMode) advised = heuristic.getAdvisableabDepth();
		else advised = heuristic.getAdvisableDepth();
		this.depth = advised;
	}

	/**
	 * @param heuristic The heuristic to use tu compute an utility value
	 * @param abMode If true, the IA will use the alpha-beta version of the minimax. False by default
	 * @param maxDepth The depth to reach. Default = value advised by the heuristic
	 */
	public BaratBuryMiniMaxAI(BaratBuryHeuristic heuristic, boolean abMode, int maxDepth){
		super();
		this.heuristic = heuristic;
		this.depth = maxDepth;
		this.alphabetaMode = abMode;
	}

	/**
	 * Init the IA with the advised depth for the minimax algorithm
	 * @param heuristic The heuristic to use tu compute the evaluation value
	 * @param abMode If true, the IA will use the alpha-beta version of the minimax
	 */
	public BaratBuryMiniMaxAI(BaratBuryHeuristic heuristic, boolean abMode){
		this(heuristic, abMode, 8);
		updateDepth();
	}

	/**
	 * Init the IA using the version of minimax for 2 players or more and using the advised depth following the heuristic
	 * @param heuristic The heuristic to use tu compute the evaluation value
	 */
	public BaratBuryMiniMaxAI(BaratBuryHeuristic heuristic){
		this(heuristic, false );
	}

	/**
	 * Clone the game to perform a simulation
	 * @param board the current board
	 */
	private Game cloneGame(Board board){
		Board cloneBoard = board.clone();
		Game game = new Game(cloneBoard, leftTokensGrantee, emptyCapture, players);
		return game;
	}

	/**
	 * Change the heuristic to use for the evaluation value
	 */
	public void setHeuristic(BaratBuryHeuristic newHeuristic){
		heuristic = newHeuristic;
		updateDepth();
	}

	@Override
	public Move pickMove(String avatar){
		Game simulation = cloneGame(this.board);
		while(simulation.getCurrentPlayer() != avatar){
			simulation.setNextPlayer();
		}
		Move a;
		if( alphabetaMode ){
			a = alphabetasearch(simulation, avatar);
		}
		else{
			a = multiminimax(simulation, avatar);
		}
		return a;
	}

	/**
	 * get all possible moves. partly Copied from RandomAI
	 * @return a list of possible moves
	 */
	public List<Move> possibleMoves(Board currentBoard, String avatar){
		ArrayList<Move> possibleMoves = new ArrayList<Move>();
		for(int i=0 ; i<currentBoard.getLength() ; i++){
			if (currentBoard.getPlayer(i).equals(avatar) && !currentBoard.isKalaha(i) && currentBoard.getPieceAt(i) > 0) {
				possibleMoves.add(new Move(i));
			}
		}
		return possibleMoves;
	}

	@Override
	public void informAvatars(List<String> avatars){
		players = new ArrayList<String>( avatars.size() );
		players.addAll(avatars);
	}

//   ###########   FUNCTIONS FOR MINIMAX WITH MORE THAN 2 PLAYERS   ###########

	private HashMap<String,Integer> initMap(){
		HashMap<String,Integer> scoreVector = new HashMap<String,Integer>(players.size());
		for(int i=0 ; i<players.size() ; i++){
			scoreVector.put( players.get(i) , MINF );
		}
		return scoreVector;
	}
	private HashMap<String,Integer> scoreMultiminimax(Game game, int maxdepth){
		String av = game.getCurrentPlayer();
		HashMap<String,Integer> scores;
		if(game.isGameEnded()){
			scores = initMap();
			for(String player : players){
				scores.put(player, MINF);
			}
			List<String> winners = game.getWinners();
			if(winners.size()==1){//if equality between 2 players, we consider nobody win
				for(String winner : winners){
					scores.put(winner, PINF);
				}
			}
			else{//but still a bit better than losing
				for(String winner : winners){
					scores.put(winner, MINF+1);
				}
			}
			return scores;
		}
		else if(maxdepth == 0){
			scores = initMap();
			for(String player : players ){
				scores.put(player , heuristic.compute(game.getBoard(), player) );
			}
			return scores;
		}
		else{
			List<Move> actions = possibleMoves(game.getBoard(), av );
			int best = MINF;
			int otherbest = PINF;
			int other;
			HashMap<String,Integer> values = null;
			int myValue;
			int depthm1 = maxdepth-1;
			scores = null;
			for(Move m : actions){
				m.apply(game);
				values = scoreMultiminimax(game, depthm1);
				m.cancel(game);
				myValue = values.get(av);
				if(myValue > best){
					other = 0;
					// if myValues is equal best, so take this move if it minimise other's evaluation value
					for(String player : players){
						if( player != av ){
							other += values.get(player);
						}
					}
					if( (myValue>best) || (myValue==best && other<otherbest) ){
						scores = values;
						otherbest = other;
						best = myValue;
					}
				}
			}
			if(scores == null){
				scores = values;
			}
			return scores;
		}
	}

	/**
	 * Apply the minimax algorithm for more than 2 players
	 * @param avatar the name of this player
	 * @return the best action following minimax algorithm
	 */
	public Move multiminimax(Game game, String avatar){
		Move best = null;
		int bestScore = MINF;
		int otherbest = PINF;
		int other;
		HashMap<String,Integer> values;
		int myValue;
		int depthm1 = depth-1;
		List<Move> actions = possibleMoves(game.getBoard(), avatar );
		for(Move m : actions){
			m.apply(game);
			values = scoreMultiminimax(game , depthm1);
			m.cancel(game);
			//affichage debug System.out.print("Vecteur|"+avatar+" qui joue");for(String p : players) System.out.print("| score de "+p+" = "+values.get(p));System.out.print("\n");
			myValue = values.get(avatar);
			if(myValue > bestScore){
					other = 0;
					// if myValues is equal best, so take this move if it minimise other's evaluation value
					for(String player : players){
						if( player != avatar ){
							other += values.get(player);
						}
					}
					if( (myValue>bestScore) || (myValue==bestScore && other<otherbest) ){
						best = m;
						otherbest = other;
						bestScore = myValue;
					}
				}
			if(best == null){
				best = actions.get(0);
			}
		}
		return best;
	}

//   ###########   FUNCTIONS FOR MINIMAX ALPHA BETA ALGORITHM   ###########

	private int continueMove(Move move, Game game, int maxDepth, String avatar, int alpha, int beta){
		move.apply(game);
		int newv = 0;
		if(game.getCurrentPlayer() == avatar){
			newv = maxvalue(game, maxDepth, avatar, alpha, beta);
		}
		else{
			newv = minvalue(game, maxDepth, avatar, alpha, beta);
		}
		move.cancel(game);
		return newv;
	}
	/**
	 * Apply the minimax algorithm with alpha-beta search
	 * @param avatar the name of this player
	 * @return the best action following minimax algorithm
	 */
	public Move alphabetasearch(Game game, String avatar){
		//like max but we return the move, not the utility value.
		Move best = null;
		int alpha = MINF;
		int beta = PINF;
		List<Move> actions = possibleMoves(game.getBoard(),avatar);
		best = actions.get(0);
		int v = MINF;
		int depthm1 = this.depth-1;
		for(Move a : actions){
			v = continueMove(a, game, depthm1, avatar, alpha, beta);
			if(v > alpha){
				alpha = v;
				best = a;
			}
		}
		return best;
	}
	/**
	 * @return MINF if the opponent win in this game, PINF if avatar win in this game, 0 else
	 */
	private int endSearch(Game game, String avatar){
		if(game.isGameEnded()){
			List<String> winners = game.getWinners();
			if(winners.contains(avatar)){
				if(winners.size()==1) return PINF;
				else return MINF+1;
			}
			else return MINF;
		}
		else return 0;
	}
	
	/**
	 * Max of the minimax algorithm
	 * @param game
	 * @param maxDepth the depth to reach
	 * @param alpha the value of the best alternative for Max
	 * @param beta the value of the best alternative for Min
	 * @return a utility value
	 */
	private int maxvalue(Game game, int maxDepth, String avatar, int alpha, int beta){
		int v = endSearch(game, avatar);
		if(v == 0){
			if(maxDepth == 0){
				v = heuristic.compute(game.getBoard(), avatar);
			}
			else{
				List<Move> actions = possibleMoves(game.getBoard(),avatar);
				int depthm1 = maxDepth-1;
				for(Move a : actions){
					v = continueMove(a, game, depthm1, avatar, alpha, beta);
					if(v >= beta) return v;
					else if(v > alpha) alpha = v;
				}
				return alpha;
			}
		}
		return v;
	}
	/**
	 * Min of the minimax algorithm
	 * @param game
	 * @param maxDepth the depth to reach
	 * @param alpha the value of the best alternative for Max
	 * @param beta the value of the best alternative for Min
	 * @return a utility value
	 */
	private int minvalue(Game game, int maxDepth, String avatar, int alpha, int beta){
		int v;
		v = endSearch(game, avatar);
		if(v == 0){
			if(maxDepth == 0){
				v = heuristic.compute(game.getBoard(), avatar);
			}
			else{
				List<Move> actions = possibleMoves(game.getBoard(),avatar);
				int depthm1 = maxDepth-1;
				for(Move a : actions){
					v = continueMove(a, game, depthm1, avatar, alpha, beta);
					v = Math.min(v , maxvalue(game, depthm1, avatar, alpha, beta));
					if(v <= alpha) return v;
					else if(v < beta) beta=v;
				}
				return beta;
			}
		}
		return v;
	}
}
