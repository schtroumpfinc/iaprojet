package Games.Kalaha.Players;

import Games.Kalaha.Move;
import Games.Kalaha.Game;
import Games.Kalaha.Boards.Board;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

/**
 * AI ending the game if possible and playing randomly otherwise.
 * Works for Uniform Boards.
 * @author Bury Jason and Barat Kevin
 */
public class BaratBuryEndAI extends Player {
	
	private HashMap<String,Integer> lengths;
	private HashMap<String,Integer> initPits=new HashMap<String,Integer>();
	private HashMap<String,Integer> kalahas=new HashMap<String,Integer>();
	private HashMap<String,Boolean> initialized=new HashMap<String,Boolean>();
	private Player second;

	/**
	 * @param secondPlayer A player that will play instead this EndAI if it cannot finish the game
	 */
	public BaratBuryEndAI(Player secondPlayer){
		super();
		this.second = secondPlayer;
	}
	
	public BaratBuryEndAI() {
		super();
		this.second = new RandomAI();
	}

	@Override
	public Move pickMove(String avatar) {
		int position,higherScore=0;
		Map<String,Integer> scores=board.getScores(leftTokensGrantee);
		Map<String,Integer> pieces=board.getSums(true,true);
		for (Integer i : scores.values()) {
			if (i>higherScore) higherScore=i;			
		}
		// play if we can and won't lose by doing so
		if (canEnd(avatar)) {
			// System.out.println("[EndAI] Yes we can end !");
			if (pieces.get(avatar)>=higherScore) {
			// return first 'full' pit starting from kalaha to initPit
				for (int i = this.kalahas.get(avatar)-1 ; i>=this.initPits.get(avatar) ; i--) {
					position=this.kalahas.get(avatar)-i;
					// System.out.println("[EndAI] Right avatar : "+board.getPlayer(i).equals(avatar));
					// System.out.println("[EndAI] Position | pieces : "+position+"|"+board.getPieceAt(i));
					if (board.getPieceAt(i)==position) {
						// System.out.println("[EndAI] Picking selected move : "+i+".");
						return new Move(i);
					}
				}
			}
			// System.out.println("[EndAI] ... but we won't.");
		}
		// System.out.println("[EndAI] letting 'second' playing");
		return second.pickMove(avatar);
	}

	/**
	 * @return True if a sequence of actions can end the game without letting the enemy player play.
	 */ 
	public boolean canEnd(String avatar) {
		
		if (this.initialized.get(avatar)==null) {
			this.initPits.put(avatar,-1);
			for (int i = 0; i < board.getLength(); ++i) {
				if (board.getPlayer(i).equals(avatar)) {
					if (board.isKalaha(i)) {
						this.kalahas.put(avatar,i);
						// this.lengths.put(avatar,i-this.initPit);
					}
					else if (this.initPits.get(avatar)==-1) 
						this.initPits.put(avatar,i);
				}
			}
			this.initialized.put(avatar,true);
			// System.out.println("[EndAI] initPit | length | kalaha : "+initPit+" | "+length+" | "+kalaha);
		}
	
		int position,pieces,stock=0;
		for (int i = this.initPits.get(avatar) ; i < this.kalahas.get(avatar) ; i++) {
			position=this.kalahas.get(avatar)-i;
			pieces=board.getPieceAt(i);
			
			// System.out.println("[EndAI] i | Position | pieces :"+i+" | "+position+" | "+pieces);
			
			if ((pieces>position) || (((stock+pieces)%position)!=0)) return false;
			
			stock+=pieces;
		}
		return true;
	}

	/**
	 * Change the player that will play if this AI don't want to play
	 */
	public void setSecondPlayer(Player newPlayer){
		second = newPlayer;
	}

	@Override
	public void informLeftTokensGrantee(Game.LeftTokensGrantee leftTokensGrantee){
		super.informLeftTokensGrantee(leftTokensGrantee);
		second.informLeftTokensGrantee(leftTokensGrantee);
	}

	@Override
	public void informEmptyCapture(boolean emptyCapture){
		super.informEmptyCapture(emptyCapture);
		second.informEmptyCapture(emptyCapture);
	}

	@Override
	public void informBoard(Board board){
		super.informBoard(board);
		second.informBoard(board);
	}

	@Override
	public void informAvatar(String avatar){
		super.informAvatar(avatar);
		second.informAvatar(avatar);
	}

	@Override
	public void informAvatars(List<String> avatars){
		players = new ArrayList<String>( avatars.size() );
		players.addAll(avatars);
		second.informAvatars(avatars);
	}

	@Override
	public void informEnd(List<String> winners){
		super.informEnd(winners);
		initPits.clear();
		kalahas.clear();
		initialized.clear();
		second.informEnd(winners);
	}
}