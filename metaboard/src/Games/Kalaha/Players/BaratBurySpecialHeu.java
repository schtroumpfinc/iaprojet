package Games.Kalaha.Players;
import Games.Kalaha.Boards.Board;

/**
 * @author Bury Jason and Barat Kevin
 */
public class BaratBurySpecialHeu extends BaratBuryHeuristic{
	private static final int kalaCoef = 5;
	private static final int pitCoef = 1;
	private static final int adverseKalaCoef = -2;
	/**
	 * The sum of tokens in pits + 3*tokens in my kalaha - 2*tokens in kahala of ennemies
	 */
	@Override
	public int compute(Board board, String avatar){
		int kalasum = 0;
		int pitsum = 0;
		int adverseKalasum = 0;
		for(int i=0 ; i<board.getLength() ; i++){
			if(board.getPlayer(i).equals(avatar)){
				if(board.isKalaha(i)) kalasum += board.getPieceAt(i);
				else pitsum += board.getPieceAt(i);
			}
			else if(board.isKalaha(i)) adverseKalasum += board.getPieceAt(i);
		}
		return kalaCoef*kalasum + pitCoef*pitsum + adverseKalaCoef*adverseKalasum;
	}
}
