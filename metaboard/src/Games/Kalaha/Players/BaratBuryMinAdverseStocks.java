package Games.Kalaha.Players;
import Games.Kalaha.Boards.Board;

/**
 * @author Bury Jason and Barat Kevin
 */
public class BaratBuryMinAdverseStocks extends BaratBuryHeuristic{
	/**
	 * Get the negative number of pieces in the kalaha of other players than avatar
	 */
	@Override
	public int compute(Board board, String avatar){
		int sum = 0;
		for(int i=0 ; i<board.getLength() ; i++){
			if(!board.getPlayer(i).equals(avatar) && board.isKalaha(i)){
				sum -= board.getPieceAt(i);
			}
		}
		return sum;
	}
}
