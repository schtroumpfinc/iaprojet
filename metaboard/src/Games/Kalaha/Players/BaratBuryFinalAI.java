package Games.Kalaha.Players;
import Games.Kalaha.Game;
import Games.Kalaha.Move;

/**
 * The final AI. For the last question of the project.
 * @author Barat Kevin and Bury Jason
 */
public class BaratBuryFinalAI extends BaratBuryEndAI{
	public BaratBuryFinalAI(){
		super( new BaratBuryMiniMaxAI(new BaratBurySpecialHeu()) );
	}

	/*
	 * Choose the heuristic to use following new rules

	private void update(){
		if(this.leftTokensGrantee==Game.LeftTokensGrantee.OWNER && !this.emptyCapture){
			ai.setHeuristic(new BaratBuryMaxStocks());
		}
		else{
			ai.setHeuristic(new BaratBurySpecialHeu());
		}
	}

	@Override
	public void informLeftTokensGrantee(Game.LeftTokensGrantee leftTokensGrantee){
		super.informLeftTokensGrantee(leftTokensGrantee);
		// update();
	}

	@Override
	public void informEmptyCapture(boolean emptyCapture) {
		super.informEmptyCapture(emptyCapture);
		// update();
	}*/
}
