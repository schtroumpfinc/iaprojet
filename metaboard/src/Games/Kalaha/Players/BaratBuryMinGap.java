package Games.Kalaha.Players;
import Games.Kalaha.Boards.Board;

/**
 * @author Bury Jason and Barat Kevin
 */
public class BaratBuryMinGap extends BaratBuryHeuristic{
	/**
	 * Get the negative number of the sum of the square of the difference between pits of avatar
	 */
	@Override
	public int compute(Board board, String avatar){
		int sum = 0;
		for(int i=0 ; i<board.getLength() ; i++){
			if(board.getPlayer(i).equals(avatar) && !board.isKalaha(i)){
				for(int j=i+1 ; j<board.getLength() ; j++){//start from i+1 to add two times the difference between pits
					if(board.getPlayer(i).equals(avatar) && !board.isKalaha(i)){
						int diff = board.getPieceAt(i) - board.getPieceAt(j);
						if(diff<0){
							diff = -diff;
						}
						diff = diff*diff;
						sum -= diff;
					}
				}
			}
		}
		return sum;
	}

	@Override
	public int getAdvisableDepth(){
		return 7;
	}

	@Override
	public int getAdvisableabDepth(){
		return 8;
	}
}
