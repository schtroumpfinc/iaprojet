package Games.Kalaha.Players;
import Games.Kalaha.Game;
import Games.Kalaha.Boards.Uniform;
import Games.Kalaha.Boards.Board;
import Games.Kalaha.GameRunner;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
/**
 * A software to test our artificial intelligence and see who is the best
 * @author Bury Jason and Barat Kevin
 */
public class BaratBuryTournament{
	private static Game.LeftTokensGrantee ltg = Game.LeftTokensGrantee.OWNER;
	private static boolean emptyCapture = true;
	private static HashMap<String,Player> aiMap;
	private static ArrayList<String> avatarList;
	private static HashMap<String,Integer> endScores;

	/**
	 * Add a player to the list of candidates
	 * @param ai the candidates
	 * @param name its name
	 */
	private static void add(Player ai, String name){
		aiMap.put(name,ai);
		avatarList.add(name);
		ai.informAvatar(name);
		ai.informLeftTokensGrantee(ltg);
		ai.informEmptyCapture(emptyCapture);
	}
	public static void main(String[] args){
		//reading parameters
		if(args.length > 0){
			if(args[0].equals("owner") || args[0].equals("1")){
				ltg = Game.LeftTokensGrantee.OWNER;
			}
			else if(args[0].equals("ender") || args[0].equals("2")){
				ltg = Game.LeftTokensGrantee.ENDER;
			}
			else if(args[0].equals("nobody") || args[0].equals("3")){
				ltg = Game.LeftTokensGrantee.NOBODY;
			}
			else{
				System.out.println("1er parametre inconnu. Doit valoir owner ou ender ou nobody");
			}
		}
		if(args.length > 1){
			if(args[1].equals("true") || args[1].equals("1") || args[1].equals("vrai")){
				emptyCapture = true;
			}
			else{
				emptyCapture = false;
				System.out.println("Regle empty capture est mis a FAUX");
			}
		}
		//
		aiMap = new HashMap<String,Player>();
		avatarList = new ArrayList<String>();
		endScores = new HashMap<String,Integer>();
		// Add here candidates
		add(new BaratBuryMiniMaxAI(new BaratBurySpecialHeu()) , "Notre special");
		add(new BaratBuryMiniMaxAI(new BaratBuryMaxStocks()) , "Mr maximise sa reserve");
		add(new BaratBuryMiniMaxAI(new BaratBuryMinAdverseStocks()) , "Mr minimise la reserve ennemie");
		add(new BaratBuryMiniMaxAI(new BaratBuryMaxCases()) , "Mr maximise ses cases");
		add(new BaratBuryMiniMaxAI(new BaratBuryMinGap()) , "Mr minimise les gaps");
		// on initialise les scores de tout le monde
		for(String avatar : avatarList){
			endScores.put(avatar,0);
		}
		// ---
		for(String avatar : avatarList){
			for(String ennemy : avatarList){
				if(avatar != ennemy){
					System.out.println(avatar+" et "+ennemy);
					ArrayList<String> currentop = new ArrayList<String>(2);
					currentop.add(avatar);
					currentop.add(ennemy);
					Board board = new Uniform(6,4, currentop);
					aiMap.get(avatar).informAvatars(currentop);
					aiMap.get(ennemy).informAvatars(currentop);
					aiMap.get(avatar).informBoard(board);
					aiMap.get(ennemy).informBoard(board);
					Game game = new Game(board , ltg , emptyCapture , currentop);
					GameRunner gr = new GameRunner(game , aiMap);
					gr.gameLoop();
					List<String> winnerList = game.getWinners();
					aiMap.get(avatar).informEnd(winnerList);
					aiMap.get(ennemy).informEnd(winnerList);
					Map scores = board.getScores(game.getLeftTokensGrantee());
					System.out.println(" ("+scores.get(avatar)+" - "+scores.get(ennemy)+")");
					if(winnerList.size() == 0){
						System.out.println(" Match nul");
					}
					else if(winnerList.size() > 1){
						System.out.println(" Égalite");
					}
					else{
						System.out.println(" Le vainqueur est "+winnerList.get(0));
						endScores.put(winnerList.get(0),endScores.get(winnerList.get(0))+1);
						if (winnerList.get(0).equals(avatar))
							endScores.put(ennemy,endScores.get(ennemy)-1);
						else
							endScores.put(avatar,endScores.get(avatar)-1);
					}
				}
			}
		}
		// affichage du résultat final
		System.out.println("Résultat final :");
		for (String avatar : avatarList) {
			System.out.println(" "+avatar+":"+endScores.get(avatar));
		}
	}
}
/* TODO lancer plusieurs tournois pour de meilleurs stats ? */
