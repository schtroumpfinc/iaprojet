package Games.Kalaha.Players;
import Games.Kalaha.Boards.Board;

/**
 * @author Bury Jason and Barat Kevin
 */
public class BaratBuryMaxCases extends BaratBuryHeuristic{
	/**
	 * Get the amount of pieces in cases of avatar
	 */
	@Override
	public int compute(Board board, String avatar){
		int sum = 0;
		for(int i=0 ; i<board.getLength() ; i++){
			if(board.getPlayer(i).equals(avatar)){
				sum += board.getPieceAt(i);
			}
		}
		return sum;
	}
}
